#!/usr/bin/env bash

sudo rm -rf source chroot buildresult && git clean . -fd && ./create_chroot.sh && ./update_key_packages_list.sh && ./find_toybox_command_candidate.sh && PACKAGE=$(./select_package.sh) && echo PACKAGE=$PACKAGE && ./get_source.sh $PACKAGE && (
    ./plain_tested.sh $PACKAGE || (
        date +%d > DATE.start && ./build_package.sh $PACKAGE noToys && date +%d > DATE.end && echo $PACKAGE $(($(cat DATE.end) - $(cat DATE.cat))) >> DURATION
       )
   ) && ./get_candidates_set.sh $PACKAGE && ./toyplace.sh && (
    echo 1 > RESULT && ./build_package.sh $PACKAGE withToys > /tmp/glue.log && echo 0 > RESULT
    grep 'Unknown option' /tmp/glue.log
    (grep 'Unknown option' /tmp/glue.log -c && BADTOY=$(grep ': Unknown option ' /tmp/glue.log | head -n1 | awk -F : '{print $1}') && echo ERROR > MODE && echo $BADTOY > forced_candidates)
    ./save_results.sh $PACKAGE $(cat RESULT)
   ) && php7.3 report.php > report.html
