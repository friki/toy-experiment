#!/bin/bash

# Abort if no args[1]
[ -z "$1" ] && exit -1

LIST="$1"

# Abort if LIST is not a regular file
[ ! -f "$1" ] && exit -1

sort -R "$LIST" | head -1
