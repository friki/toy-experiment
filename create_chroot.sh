#!/bin/bash

CHROOT=./chroot
SUITE=sid

# Happily exit if list already exists
[ -e "$CHROOT" ] && exit 0

# Create into a reusable path
[ ! -d "$CHROOT"-bkp ] && \
    sudo mmdebstrap --include="cowdancer build-essential aptitude" $SUITE "$CHROOT"-bkp &&
    sudo chroot "$CHROOT"-bkp aptitude update

# Recover from reusable path to final destination
sudo rm -rf "$CHROOT" && \
    sudo cp -al "$CHROOT"-bkp "$CHROOT"
