#!/bin/bash

# Abort if no args[1]
[ -z "$1" ] && exit -1

PACKAGE=$1

echo REFINE > MODE
echo 'SELECT toy FROM result WHERE result=1 AND package="'"$PACKAGE"'" ORDER BY RANDOM() LIMIT (SELECT (COUNT(toy)+1)/2 FROM result WHERE result=1 AND package="'"$PACKAGE"'")' | \
    sqlite3 results.db > forced_candidates

wc -l forced_candidates

grep -c '' forced_candidates && exit

echo EXPLORE > MODE
echo 'SELECT toy FROM (SELECT toy FROM result GROUP BY toy HAVING SUM(result)=0) AS a LEFT JOIN (SELECT toy FROM result WHERE package="'"$PACKAGE"'") AS b USING(toy)WHERE b.toy IS NULL' | \
    sqlite3 results.db > forced_candidates

wc -l forced_candidates

grep -c '' forced_candidates && exit

echo EXPLORE > MODE
echo 'SELECT toy FROM (SELECT toy FROM result WHERE result=0 GROUP BY toy) AS a LEFT JOIN (SELECT package, toy FROM result WHERE package="'"$PACKAGE"'") AS b USING(toy) WHERE b.package IS NULL ORDER BY RANDOM() LIMIT 10' | \
    sqlite3 results.db > forced_candidates

wc -l forced_candidates

#echo 'SELECT toy FROM (SELECT toy, COUNT(toy) AS c FROM result WHERE result=0 GROUP BY toy ORDER BY 2 ASC, RANDOM()) LIMIT 5;' | \
#    sqlite3 results.db > forced_candidates
#
#echo 'SELECT toy FROM (SELECT toy, COUNT(toy) AS c FROM result WHERE result=0 GROUP BY toy ORDER BY 2 DESC, RANDOM() LIMIT (SELECT MAX(70, COUNT(DISTINCT(package))) FROM result));' | \
#    sqlite3 results.db >> forced_candidates
