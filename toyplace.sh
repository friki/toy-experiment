#!/bin/bash

# Purpose: Replace args[1] command using toy from toybox

## Abort if no args[1]
#[ -z "$1" ] && exit -1

CHROOT=./chroot
#COMMAND=$1
FORCED_CANDIDATES=forced_candidates
t=toybox
T=/bin/"$t"

## Abort if COMMAND is not a regular file
#COMMAND_LOCATION="$CHROOT/$COMMAND"
#[ ! -f "$COMMAND_LOCATION" ] && exit -1

# Copy toybox binary
[ ! -f "$CHROOT/$TOYBOX/bin/" ] && sudo cp -a "$T" "$CHROOT/$TOYBOX/bin/"

# Symlink toy
#sudo ln -sf "$T" "$COMMAND_LOCATION"

# Add links for all forced candidates
cat "$FORCED_CANDIDATES" | while read FORCED_CANDIDATE
do
    sudo ln -sf "$T" "$CHROOT/$FORCED_CANDIDATE"
done
