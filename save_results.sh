#!/bin/bash

# Abort if no args[1]
[ -z "$1" ] && exit -1

PACKAGE=$1

MODE=$(cat MODE)
RESULT=$(cat RESULT)

SQLITE=sqlite3

echo SAVE: PACKAGE=$PACKAGE MODE=$MODE RESULT=$RESULT

if [ "$MODE" = "REFINE" ]
then
    if [ "$RESULT" = "0" ]
    then
        for toy in $(cat forced_candidates)
        do
            echo 'REPLACE INTO result VALUES ("'"$PACKAGE"'", "'"$toy"'", '$RESULT')' | $SQLITE results.db
        done
    else
        echo 'UPDATE result SET result=3 WHERE package="'"$PACKAGE"'" AND result=1' | $SQLITE results.db
        for toy in $(cat forced_candidates)
        do
            echo 'REPLACE INTO result VALUES ("'"$PACKAGE"'", "'"$toy"'", '$RESULT')' | $SQLITE results.db
        done
        echo 'DELETE FROM result WHERE result=3' | $SQLITE results.db
        echo 'UPDATE result set result=(SELECT CASE WHEN COUNT(package)=1 THEN 2 ELSE 1 END FROM result WHERE package="'"$PACKAGE"'" AND result=1) WHERE package="'"$PACKAGE"'" AND result=1' | $SQLITE results.db
    fi
elif [ "$MODE" = "EXPLORE" ]
then
    for toy in $(cat forced_candidates)
    do
        echo 'REPLACE INTO result VALUES ("'"$PACKAGE"'", "'"$toy"'", '$RESULT')' | $SQLITE results.db
    done
elif [ "$MODE" = "ERROR" ]
then
    cat forced_candidates
    for toy in $(cat forced_candidates)
    do
        echo 'REPLACE INTO result VALUES ("'"$PACKAGE"'", (SELECT toy FROM result WHERE toy LIKE "%/'"$toy"'" LIMIT 1), 2)'
        echo 'REPLACE INTO result VALUES ("'"$PACKAGE"'", (SELECT toy FROM result WHERE toy LIKE "%/'"$toy"'" LIMIT 1), 2)' | $SQLITE results.db
    done
    echo 'DELETE FROM result WHERE result=1 AND package="'"$PACKAGE"'"' | $SQLITE results.db
fi
