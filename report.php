<?php

$sql = 'SELECT * FROM result ORDER BY package, toy, result';

$filename = 'results.db';
$db = new SQLite3($filename);
$result = $db->query($sql);

$packages = [];
$toys = [];
$results = [];
while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
    $package = $row['package'];
    $toy = $row['toy'];
    $r = $row['result'];

    $packages[$package] = $package;
    $toys[$toy] = $toy;

    if (!array_key_exists($package, $results)) {
        $results[$package] = [];
    }
    $results[$package][$toy] = (int) $r;
}

sort($packages);
sort($toys);




echo "<h1>Toybox replacing compatibility experiment - Debian buildpackage's</h1>\n";
echo "<h2>Why experimenting ?</h2>\n";
?>
<pre>
Because I opened an ITP [0] at Debian and I need some data to decide which toys
should be included and to arguing the Toybox benefits for Debian to get
Sponsored. Also to plan next steps adapting both projects to each other (Debian
and Toybox).
[0] https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=968162
</pre>
<?php
echo "<h2>Build environment</h2>\n";
?>
<pre>
The packages are build using gbp-buildpackage, pbuilder and a chroot generated
with mmdebstrap.
The source code is downloaded using 'gbp clone', then only packages providing
d/control 'Vcs-Git' field are considered in first place.
After cloning the package it's intended to build. If it fails, we drop the
package from the list and it's not present in the report below.
After a 'clean' build, I've replaced Debian binary files in chroot by Toybox
commands (toys) and rebuild many times using different subset of toys to fill
the tables below.
</pre>
<?php
echo "<h2>Quick summary</h2>\n";
?>
<pre>
206 toys seems to be good enough to replace original Debian binary (in my tiny
experiment using 52 random Debian key-packages).

In the counter part 18 toys are found to cause incidents when replacing the
original Debian binary command files. Problems can be caused on building
scripts, dependency installation or building the code. Autopkgtests still to be
added.

All problems I've traced are due to unimplemented command options (or
non-available long alias).
For instance 'chown' causes most detected incidents because lacks from
'--no-dereference' option.

Replaceable parts comes from different packages. As some binary files can be
provided by more than one package this list is incomplete and approximated:
  76 coreutils
  24 util-linux
  11 procps
   5 mount
   5 kmod
   4 systemd-*
   3 net-tools
   3 grep
   ...
</pre>
<?php
echo "<h2>Show me the code!</h2>\n";
?>
<pre>
Sure!
It's hosted on my salsa.d.o repositories group:
  https://salsa.debian.org/friki/toy-experiment

It's in a draft-stage. It means undocumented dirty code sketches in addition to
bugs and dragons.
</pre>
<?php
echo "<h2>Problems focused summary</h2>\n";
?>
<pre>
Legend:
</pre>
<table>
<tr><td style="min-width:30px" bgcolor="green"></td><td>Packet on the left can be built with toy in the top without detected incidents</td></tr>
<tr><td style="min-width:30px" bgcolor="orange"></td><td>Temporary state. More rebuilds are required to find out what of orange toys are causing problems</td></tr>
<tr><td style="min-width:30px" bgcolor="red"></td><td>Packet on the left can't be built with toy in the top without applying patches</td></tr>
</table>
<?php
$error_packages = [];
$error_toys = [];
foreach ($packages as $package) {
    foreach ($toys as $toy) {
        if (array_key_exists($toy, $results[$package]) && $results[$package][$toy]) {
            $error_packages[$package] = $package;
            $error_toys[$toy] = $toy;
        }
    }
}
sort($error_packages);
sort($error_toys);
echo "<table border=1>\n";
echo " <tr>";
echo "<th style=\"min-width:300px\">package\\toy</th>";
foreach ($error_toys as $toy) {
    echo "<th>$toy</th>";
}
echo "</tr>\n";

foreach ($error_packages as $package) {
    echo "  <tr><th>$package</th>";
    foreach ($error_toys as $toy) {
        if (array_key_exists($toy, $results[$package])) {
            $r = $results[$package][$toy];
            $color = ($r==0)?'green':(($r==1)?'orange':'red');
            echo "<td bgcolor=$color></td>";
        } else {
            echo "<td></td>";
        }
    }
    echo "</tr>\n";
}
echo "</table>\n";




echo "<h2>Full report</h2>\n";
?>
<pre>
Legend:
</pre>
<table>
<tr><td style="min-width:30px" bgcolor="green"></td><td>Packet on the left can be built with toy in the top without detected incidents</td></tr>
<tr><td style="min-width:30px" bgcolor="orange"></td><td>Temporary state. More rebuilds are required to find out what of orange toys are causing problems</td></tr>
<tr><td style="min-width:30px" bgcolor="red"></td><td>Packet on the left can't be built with toy in the top without applying patches</td></tr>
</table>
<?php
echo "<table border=1>\n";
echo " <tr>";
echo "<th style=\"min-width:300px\">package\\toy</th>";
foreach ($toys as $toy) {
    echo "<th>$toy</th>";
}
echo "</tr>\n";

foreach ($packages as $package) {
    echo "  <tr><th>$package</th>";
    foreach ($toys as $toy) {
        if (array_key_exists($toy, $results[$package])) {
            $r = $results[$package][$toy];
            $color = ($r==0)?'green':(($r==1)?'orange':'red');
            echo "<td bgcolor=$color></td>";
        } else {
            echo "<td></td>";
        }
    }
    echo "</tr>\n";
}
echo "</table>\n";
