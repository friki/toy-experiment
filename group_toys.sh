#!/usr/bin/env bash

mkdir toys_n_packages

for i in $(toybox --long)
do
    TOY="/$i"
    CONFLICTING_PACKAGE=$(apt-file search /$i | grep " /$i$" | awk -F : '{print $1}' | head -1)

    [ ! -z "$CONFLICTING_PACKAGE" ] && \
        mkdir -p toys_n_packages/"$CONFLICTING_PACKAGE"/"$TOY"/ && \
        touch toys_n_packages/"$CONFLICTING_PACKAGE"/"$TOY"/.placeholder
done
