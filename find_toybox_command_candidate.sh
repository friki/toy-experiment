#!/bin/bash

# Look for comands into chroot that can be replaced with compiled

t=toybox
T=/bin/$t

CHROOT=./chroot
COMMANDS=commands.list
CANDIDATES=candidate_toys.list

# Happily exit if list already exists
[ -e "$CANDIDATES" ] && exit 0

# Generate chroot commands list
BASE=$(pwd)
pushd "$CHROOT"
sudo $t find . -type f | $t grep bin/ | $t sort > "$BASE/$COMMANDS"
popd

# Grep for matches
for i in `toybox`
do
    $t grep -w $i$ "$COMMANDS"
done | $t cut -b 2- > "$CANDIDATES"

rm "$COMMANDS"
