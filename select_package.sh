#!/usr/bin/env bash

#./pick_one.sh key_packages.list

PACKAGE=$(echo 'SELECT package FROM result WHERE result=1 GROUP BY package ORDER BY COUNT(package) LIMIT 1' | sqlite3 results.db)

[ ! -z "$PACKAGE" ] && echo $PACKAGE && exit

PACKAGE=$(echo 'SELECT package FROM result GROUP BY package ORDER BY COUNT(package) LIMIT 1' | sqlite3 results.db)

[ ! -z "$PACKAGE" ] && echo $PACKAGE && exit

echo ALL_DONE_STOP_NOW
sleep 10
exit -1
